from pyes import ES
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.schema import MetaData

from config.settings import settings

metadata = MetaData()
Session = scoped_session(sessionmaker())
Base = declarative_base(metadata=metadata)


def get_objects_list(session, model, **kwargs):
    return session.query(model).filter_by(**kwargs)

###############################################################################
# function below are duplicates of functions from fitch.db,
# but not related on Pyramid configuration
###############################################################################


def get_object(session, model, **kwargs):
    return session.query(model).filter_by(**kwargs).first()


def get_pyes_conn():
    return ES(
        server=settings['fitch.es_hosts_as_payes_server_list'],
        timeout=None,
        default_indices=[settings['fitch.es_index']],
        default_types=['item'])


def setup_sqla():
    engine = create_engine(settings['sqlalchemy.url'])
    Session.configure(bind=engine)
    Base.metadata.bind = engine


setup_sqla()
