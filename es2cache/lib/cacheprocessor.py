import redis
import json

class CacheProcessor(object):
    
    def __init__(self, host='localhost', port=6379, db=0,
                       *args, **kwargs):
        self.redis = redis.Redis(host=host, port=port, db=db, *args, **kwargs)
        
    def save(self, key, value):
        self.redis.set(key, value)
        return self.redis.pipeline().execute()
