#!/usr/bin/python

import os
import sys

###############################################################################
# NOTE: this should be removed after integration to Fitch
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from config.settings import settings
for name in os.listdir(settings['fitch.eggs_path']):
    sys.path.append(os.path.join(settings['fitch.eggs_path'], name))  # dirty
###############################################################################

from datetime import datetime, timedelta

from saferedisqueue import SafeRedisQueue
from lib.cacheprocessor import CacheProcessor

from fitch.models.query import FerretQuery
from fitch.models.account import Account as FerretAccount

from lib.db import Session, get_object, get_objects_list
from models.dashboard import Dashboard
from config.settings import settings

try:
    queue = SafeRedisQueue(host=settings['fitch.queue_redis_host'],
                           port=settings['fitch.queue_redis_port'],
                           db=settings['fitch.queue_redis_database'])
    uid, account_id = queue.pop()
    if uid is None:
        exit(0)

    account = get_object(Session(), FerretAccount, id=account_id)
    if not account:
        raise

    queries = get_objects_list(Session(), FerretQuery, account_id=account_id)

    now = datetime.now()
    dates = {
        # TODO: right now account has no last-login timestamp, so here we use
        # now - 1 hour. This must be changed
        "since-last_login": now - timedelta(hours=1),
        "since-today": now.replace(hour=0, minute=0),
        "since-30-days": (now - timedelta(days=30)).replace(hour=0, minute=0)
    }

    dashboard = Dashboard(account=account, queries=queries, dates=dates)
    unified_json = dashboard.execute(to_json=True)

    cache = CacheProcessor(host=settings['fitch.cache_redis_host'],
                           port=settings['fitch.cache_redis_port'],
                           db=settings['fitch.cache_redis_database'])
    cache.save(key=account_id, value=unified_json)

# TODO: need to handle ES/Psql/Redis exceptions as temporary or permanent
except:
    if 'queue' in vars():
        queue.fail(uid)
    raise
else:
    queue.ack(uid)
