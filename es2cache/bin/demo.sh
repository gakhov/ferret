#!/usr/bin/python

import os
import sys
import argparse

arg_parser = argparse.ArgumentParser(
    description='Make ES search for the dashboard queries', 
    epilog='Reads integer account ID from the standard input.'
)
arg_parser.add_argument(
    'id', 
    type=int, 
    nargs="+", 
    help='Account ID for quering'
)
args = arg_parser.parse_args()

###############################################################################
# NOTE: this should be removed after integration to Fitch
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from config.settings import settings
for name in os.listdir(settings['fitch.eggs_path']):
    sys.path.append(os.path.join(settings['fitch.eggs_path'], name)) # dirty
###############################################################################

from datetime import datetime, timedelta

from fitch.models.query import FerretQuery
from fitch.models.account import Account as FerretAccount
from lib.db import Session, get_object, get_objects_list
from models.dashboard import Dashboard

try:
    account_id = args.id[0] if len(args.id) > 0 else -1
    account = get_object(Session(), FerretAccount, id=account_id)
    if not account:
        raise

    queries = get_objects_list(Session(), FerretQuery, account_id=account_id)

    now = datetime.now()
    dates = {
       # TODO: right now account has no last-login timestamp, so here we use
       # now - 1 hour. This must be changed
       "since-last-login": (now - timedelta(hours=1)),
       "since-today": now.replace(hour=0, minute=0),
       "since-30-days": (now - timedelta(days=30)).replace(hour=0, minute=0)
    }
 
    dashboard = Dashboard(account=account, queries=queries, dates=dates)
    unified_json = dashboard.execute(to_json=True)
    print unified_json

# TODO: need to handle ES/Psql/Redis exceptions as temporary or permanent
except:
    # TODO: print exception message
    raise
finally:
    pass
