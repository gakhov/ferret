import heapq
from abc import ABCMeta, abstractmethod
from collections import defaultdict
from datetime import datetime
from operator import itemgetter

from pyes.utils import ESRange
from pyes.filters import RangeFilter

from fitch.models.query import get_item_headline, get_item_abstract
from fitch.models.query import QUERY_SORT_MAPPINGS

# TODO: After integration into the Fitch project
# these modules could be replaced
# lib.db -> fitch.db
from lib.db import get_pyes_conn


# TODO: find an appropriate place for this function
def get_range_filter(date=None):
    """ Returns a PyES ANDFilter with RangeFilter based on `date` param """
    start_date = date if date else datetime.now()
    return RangeFilter(
        ESRange('item_time',
                start_date.strftime("%Y-%m-%dT%H:%M:00.000000Z"),
                include_lower=True))


class Widget(object):
    """
    Basic abstract class for widgets
    """

    __metaclass__ = ABCMeta
    es_conn = None

    def __init__(self, queries=[]):
        self.queries = queries

    @property
    def connection(self):
        if not self.es_conn:
            self.es_conn = get_pyes_conn()
        return self.es_conn

    def search(self, size=10, date=None, to_json=False):
        # TODO: looks like PyES doesn't support MultiSearch.
        # This function should be rewritten if multisearch
        # solution found.
        results = []
        for (uq, sq) in self._es_search_queries(size, date):
            rs = self.connection.search(query=sq,
                                        doc_types=self.connection.default_types)
            results.extend(
                self._format_search_result(rs, uq))

        return self._sort_and_format(results)

    @abstractmethod
    def _es_search_queries(self, size=10):
        pass

    @abstractmethod
    def _format_search_result(self, es_response, associated_query):
        pass

    @abstractmethod
    def _sort_and_format(self, data, **kwargs):
        pass


class NewestArticlesWidget(Widget):
    """ Widget for getting newest articles for users' queries  """

    FETCH_FIELDS = [
        'content.title',
        'content.description',
        'content.descfromheadlines',
        'content.description',
        'content.body_cleaned',
        'content.body',
        'hostname_info.real_name',
        'item_time'
    ]

    def _es_search_queries(self, size=10, date=None):
        for query in self.queries:
            search = query.to_es_search(size=size, add_facets=True,
                                        facet_totals=False,
                                        sorting='time')
            search.fields = self.FETCH_FIELDS
            search.filter.filters.append(get_range_filter(date))
            yield (query, search)

    def _format_search_result(self, es_response, associated_query):
        formatted_records = []
        for item in es_response:
            record = {
                'query_id': associated_query.id,
                'headline': get_item_headline(item),
                'description': get_item_abstract(item),
                'real_name': item.get('hostname_info.real_name', ''),
                'item_time': item.get('item_time', '')
            }

            formatted_records.append(record)

        return formatted_records

    def _sort_and_format(self, data, **kwargs):
        return data


class GenreDistributionWidget(Widget):
    """ Widget for getting genre distribution for users' queries  """

    FETCH_FIELDS = None

    def _es_search_queries(self, size=10, date=None):
        for query in self.queries:
            search = query.to_es_search(size=size, add_facets=False,
                                        facet_totals=False,
                                        sorting='time')
            search.fields = self.FETCH_FIELDS
            search.facet.add_term_facet(
                name="genres",
                field="hostname_info.genre",
                order="count",
                size=size
            )
            search.size = None
            search.filter.filters.append(get_range_filter(date))
            yield (query, search)

    def _format_search_result(self, es_response, associated_query):
        records = {}
        terms = es_response.facets['genres']['terms']
        for term in terms:
            genre = term['term']
            if genre not in records:
                records[genre] = 0

            records[genre] += term['count']

        formatted_records = []
        for (genre, count) in records.iteritems():
            formatted_records.append({
                'genre': genre,
                'sum_count': count
            })
        return formatted_records

    def _sort_and_format(self, data, **kwargs):
        data.sort(key=itemgetter('sum_count'), reverse=True)
        return data


class ActiveCitiesWidget(Widget):
    """ Widget for getting most active cities for users' queries  """

    FETCH_FIELDS = None

    def _es_search_queries(self, size=10, date=None):
        for query in self.queries:
            search = query.to_es_search(size=size, add_facets=False,
                                        facet_totals=False,
                                        sorting='time')
            search.fields = self.FETCH_FIELDS
            search.facet.add_term_facet(
                name="cities",
                field="geo.city",
                order="count",
                size=size
            )
            search.size = None
            search.filter.filters.append(get_range_filter(date))
            yield (query, search)

    def _format_search_result(self, es_response, associated_query):
        records = {}
        terms = es_response.facets['cities']['terms']
        for term in terms:
            city = term['term']
            if city not in records:
                records[city] = 0

            records[city] += term['count']

        formatted_records = []
        for (city, count) in records.iteritems():
            formatted_records.append({
                'city': city,
                'sum_count': count
            })

        return formatted_records

    def _sort_and_format(self, data, **kwargs):
        data.sort(key=itemgetter('sum_count'), reverse=True)
        return data


class ActiveQueriesWidget(Widget):
    """ Widget for getting most active users' queries  """

    FETCH_FIELDS = None

    def _es_search_queries(self, size=10, date=None):
        for query in self.queries:
            search = query.to_es_search(size=size, add_facets=False,
                                        facet_totals=False,
                                        sorting='time')
            search.fields = self.FETCH_FIELDS
            search.size = None
            search.filter.filters.append(get_range_filter(date))
            yield (query, search)

    def _format_search_result(self, es_response, associated_query):
        return [{
            'query_id': associated_query.id,
            'hit_count': es_response.total}]

    def _sort_and_format(self, data, **kwargs):
        data.sort(key=itemgetter('hit_count'), reverse=True)
        return data


class NegativeQueriesWidget(Widget):
    """ Widget for getting most negative users' queries  """

    FETCH_FIELDS = None
    QUERY_FIELDS = [
        'sentiment.neg'
    ]

    def _es_search_queries(self, size=10, date=None):
        for query in self.queries:
            query_kwargs = {"fields": self.QUERY_FIELDS}
            search = query.to_es_search(size=size, add_facets=False,
                                        facet_totals=False,
                                        sorting='time',
                                        query_kwargs=query_kwargs)
            search.fields = self.FETCH_FIELDS
            search.size = None
            search.filter.filters.append(get_range_filter(date))
            yield (query, search)

    def _format_search_result(self, es_response, associated_query):
        return [{
            'query_id': associated_query.id,
            'hit_count': es_response.total}]

    def _sort_and_format(self, data, **kwargs):
        data.sort(key=itemgetter('hit_count'), reverse=True)
        return data


class ImportantTopicsWidget(Widget):
    """ Widget for getting most important topic from users' queries  """

    FETCH_FIELDS = None

    def _es_search_queries(self, size=10, date=None):
        for query in self.queries:
            search = query.to_es_search(size=size, add_facets=False,
                                        facet_totals=False,
                                        sorting='time')
            search.fields = self.FETCH_FIELDS

            search.facet.add_term_facet(
                name="kw",
                field="kw.lemma",
                order="count",
                size=size
            )

            search.facet.add_term_facet(
                name="kwpos",
                field="sentiment.kw.pos.lemma",
                order="count",
                size=size
            )

            search.facet.add_term_facet(
                name="kwneg",
                field="sentiment.kw.neg.lemma",
                order="count",
                size=None
            )
            search.size = None
            search.filter.filters.append(get_range_filter(date))
            yield (query, search)

    def _format_search_result(self, es_response, associated_query):
        formatted_records = defaultdict(list)

        if 'kw' in es_response.facets:
            terms = es_response.facets['kw']['terms']
            for term in terms:
                formatted_records["all"].append({
                    "query_id": associated_query.id,
                    "keyword": term['term'],
                    "count": term['count']
                })

        if 'kwpos' in es_response.facets:
            terms = es_response.facets['kwpos']['terms']
            for term in terms:
                formatted_records["positive"].append({
                    "query_id": associated_query.id,
                    "keyword": term['term'],
                    "count": term['count']
                })

        if 'kwneg' in es_response.facets:
            terms = es_response.facets['kwneg']['terms']
            for term in terms:
                formatted_records["negative"].append({
                    "query_id": associated_query.id,
                    "keyword": term['term'],
                    "count": term['count']
                })

        return [formatted_records]

    def _sort_and_format(self, data, **kwargs):
        results = defaultdict(list)
        for item in data:
            results["all"].extend(item["all"])
            results["positive"].extend(item["positive"])
            results["negative"].extend(item["negative"])

        results["all"].sort(key=itemgetter('count'), reverse=True)
        results["positive"].sort(key=itemgetter('count'), reverse=True)
        results["negative"].sort(key=itemgetter('count'), reverse=True)
        return results


class ImportantThemeclustersWidget(Widget):
    """ Widget for getting most important themeclusters for users' queries  """
    FETCH_FIELDS = None

    DETAILED_FETCH_FIELDS = [
        'content.title',
        'content.description',
        'content.descfromheadlines',
        'content.description',
        'content.body_cleaned',
        'content.body',
        'hostname_info.real_name',
        'item_time'
    ]

    def search(self, size=10, date=None, to_json=False):
        # first, get clusters
        biggest_clusters_results = []
        for (uq, sq) in self._es_search_queries(size, date):
            rs = self.connection.search(query=sq,
                                        doc_types=self.connection.default_types)
            biggest_clusters_results.extend(
                self._format_search_result(rs, uq))

        important_results = heapq.nlargest(size, biggest_clusters_results,
                                   key=itemgetter('cluster_count'))
        # then get recent queries from most valued clusters
        results = []
        for (cluster, sq) in self._es_cluster_search(important_results, 1, date):
            rs = self.connection.search(query=sq,
                                        doc_types=self.connection.default_types)
            results.extend(
                self._format_clusters_results(rs, cluster))

        return self._sort_and_format(results)

    def _es_search_queries(self, size=10, date=None):
        for query in self.queries:
            search = query.to_es_search(size=size, add_facets=False,
                                        facet_totals=False,
                                        sorting='time')
            search.fields = self.FETCH_FIELDS
            # get most valued cluster (order=count, size=1)
            search.facet.add_term_facet(
                name="clusters",
                field="content.cluster_id",
                order="count",
                size=1,
                is_global=False
            )
            search.size = None
            search.filter.filters.append(get_range_filter(date))
            yield (query, search)

    def _es_cluster_search(self, cluster_results, size=1, date=None):
        for cl in cluster_results:
            query = [q for q in self.queries if q.id == cl.get('query_id')][0]
            search = query._get_cluster_search(cluster_id=cl['cluster_id'],
                                               size=size)
            search.sort = QUERY_SORT_MAPPINGS['score']
            search.fields = self.DETAILED_FETCH_FIELDS
            search.filter.filters.append(get_range_filter(date))
            yield (cl, search)

    def _format_search_result(self, es_response, associated_query):
        formatted_records = []

        if 'clusters' in es_response.facets:
            terms = es_response.facets['clusters']['terms']
            for term in terms:
                formatted_records.append({
                    "query_id": associated_query.id,
                    "cluster_id": term['term'],
                    "cluster_count": term['count'],
                })
        return formatted_records

    def _format_clusters_results(self, es_response, associated_cluster):
        formatted_records = []

        for item in es_response:
            formatted_records.append({
                'query_id': associated_cluster['query_id'],
                'cluster_id': associated_cluster['cluster_id'],
                'cluster_count': associated_cluster['cluster_count'],
                'headline': get_item_headline(item),
                'description': get_item_abstract(item),
                'real_name': item.get('hostname_info.real_name', ''),
                'item_time': item.get('item_time', '')
            })
        return formatted_records

    def _sort_and_format(self, data, **kwargs):
        data.sort(key=itemgetter('cluster_count'), reverse=True)
        for item in data:
            del item['cluster_count']
        return data
