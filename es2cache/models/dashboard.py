import json
from collections import defaultdict
from models import widget


class Dashboard(object):
    def __init__(self, account, queries, dates):
        self.account = account
        self.queries = queries
        self.dates = dates

    def execute(self, to_json=True):
        result = {}
        try:
            result = self._execute_queries()
        except:
            # TODO: check if exception is temporary (like connection issue)
            # or permanent (like syntax issue)
            raise
        return json.dumps(result) if to_json else result

    def _execute_queries(self):
        result = defaultdict(dict)

        # create required widgets
        active_cities = widget.ActiveCitiesWidget(self.queries)
        active_queries = widget.ActiveQueriesWidget(self.queries)
        genre_distribution = widget.GenreDistributionWidget(self.queries)
        important_clusters = widget.ImportantThemeclustersWidget(self.queries)
        important_topics = widget.ImportantTopicsWidget(self.queries)
        newest_articles = widget.NewestArticlesWidget(self.queries)
        negative_queries = widget.NegativeQueriesWidget(self.queries)

        for period, date in self.dates.iteritems():
            result[period].update({
                "newest-article": newest_articles.search(
                    size=10,
                    date=date),
                "genre": genre_distribution.search(
                    size=10,
                    date=date),
                "city": active_cities.search(
                    size=10,
                    date=date),
                "most-active-query": active_queries.search(
                    size=10,
                    date=date),
                "most-negative-query": negative_queries.search(
                    size=10,
                    date=date),
                 "topics": important_topics.search(
                    size=10,
                    date=date),
                 "most-important": important_clusters.search(
                    size=10,
                    date=date)
            })

        return result
