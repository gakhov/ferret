import os
import sys
import ConfigParser


class CustomSettings(object):
    """ Emulation Class for real settings behaviour of the Fitch project """
    def __init__(self, path, default_section='custom'):
        if not os.path.isfile(path):
            raise IOError("Configuration file '%s' not found" % path)
        self.settings = ConfigParser.ConfigParser()
        self.settings.read(path)
        self.default_section = default_section

    def   __getitem__(self, key):
        try:
            value = self.settings.getint(self.default_section, key)
        except:
            value = self.settings.get(self.default_section, key)
        return value


cfg_path = os.path.join(
    os.path.abspath(os.path.dirname(__file__)),
    'settings.cfg')
settings = CustomSettings(cfg_path, 'custom')
